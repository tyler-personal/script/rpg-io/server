(set-option :auto-config false)
(set-option :model true)
(set-option :model.partial false)

(set-option :smt.mbqi false)
(define-sort Str () Int)
(declare-fun strLen (Str) Int)
(declare-fun subString (Str Int Int) Str)
(declare-fun concatString (Str Str) Str)
(define-sort Elt () Int)
(define-sort Set () (Array Elt Bool))
(define-fun smt_set_emp () Set ((as const Set) false))
(define-fun smt_set_mem ((x Elt) (s Set)) Bool (select s x))
(define-fun smt_set_add ((s Set) (x Elt)) Set (store s x true))
(define-fun smt_set_cup ((s1 Set) (s2 Set)) Set ((_ map or) s1 s2))
(define-fun smt_set_cap ((s1 Set) (s2 Set)) Set ((_ map and) s1 s2))
(define-fun smt_set_com ((s Set)) Set ((_ map not) s))
(define-fun smt_set_dif ((s1 Set) (s2 Set)) Set (smt_set_cap s1 (smt_set_com s2)))
(define-fun smt_set_sub ((s1 Set) (s2 Set)) Bool (= smt_set_emp (smt_set_dif s1 s2)))
(define-sort Map () (Array Elt Elt))
(define-fun smt_map_sel ((m Map) (k Elt)) Elt (select m k))
(define-fun smt_map_sto ((m Map) (k Elt) (v Elt)) Map (store m k v))
(define-fun smt_map_cup ((m1 Map) (m2 Map)) Map ((_ map (+ (Elt Elt) Elt)) m1 m2))
(define-fun smt_map_def ((v Elt)) Map ((as const (Map)) v))
(define-fun bool_to_int ((b Bool)) Int (ite b 1 0))
(define-fun Z3_OP_MUL ((x Int) (y Int)) Int (* x y))
(define-fun Z3_OP_DIV ((x Int) (y Int)) Int (div x y))
(declare-fun runFun () Int)
(declare-fun lq_tmp$36$x$35$$35$380 () Int)
(declare-fun cast_as_int () Int)
(declare-fun VV$35$$35$1335 () Int)
(declare-fun lq_anf$36$$35$$35$7205759403792797118$35$$35$dUK () Int)
(declare-fun fix$36$$36$krep_aTV () Int)
(declare-fun lq_anf$36$$35$$35$7205759403792797110$35$$35$dUC () Int)
(declare-fun addrLen () Int)
(declare-fun GHC.Types.$36$tcChar () Int)
(declare-fun papp5 () Int)
(declare-fun x_Tuple21 () Int)
(declare-fun x_Tuple65 () Int)
(declare-fun VV$35$$35$1196 () Int)
(declare-fun lq_tmp$36$x$35$$35$426 () Int)
(declare-fun x_Tuple55 () Int)
(declare-fun lq_tmp$36$x$35$$35$606 () Int)
(declare-fun Game.$36$tcRole () Int)
(declare-fun x_Tuple33 () Int)
(declare-fun GHC.Types.$36$tcInt () Int)
(declare-fun GHC.Types.LT () Int)
(declare-fun x_Tuple77 () Int)
(declare-fun fix$36$$36$krep_aU4 () Int)
(declare-fun lq_tmp$36$x$35$$35$1001 () Int)
(declare-fun Game.$36$tc$39$Wizard () Int)
(declare-fun GHC.Types.$36$tc$91$$93$ () Int)
(declare-fun lq_tmp$36$x$35$$35$580 () Int)
(declare-fun papp3 () Int)
(declare-fun x_Tuple63 () Int)
(declare-fun x_Tuple41 () Int)
(declare-fun lq_anf$36$$35$$35$7205759403792797137$35$$35$dV3 () Int)
(declare-fun lq_anf$36$$35$$35$7205759403792797130$35$$35$dUW () Int)
(declare-fun fix$36$$36$krep_aTW () Int)
(declare-fun GHC.Types.$58$ () Int)
(declare-fun VV$35$$35$1509 () Int)
(declare-fun GHC.Types.krep$36$$42$ () Int)
(declare-fun papp4 () Int)
(declare-fun GHC.Types.Module () Int)
(declare-fun lq_tmp$36$x$35$$35$992 () Int)
(declare-fun lq_tmp$36$x$35$$35$486 () Int)
(declare-fun x_Tuple64 () Int)
(declare-fun VV$35$$35$1316 () Int)
(declare-fun lit$36$$39$Healer () Str)
(declare-fun GHC.Types.KindRepFun () Int)
(declare-fun Game.$36$trModule () Int)
(declare-fun Game.$36$tcPlayer () Int)
(declare-fun lq_tmp$36$x$35$$35$700 () Int)
(declare-fun fix$36$$36$krep_aU3 () Int)
(declare-fun fix$36$$36$krep_aTZ () Int)
(declare-fun VV$35$$35$1661 () Int)
(declare-fun autolen () Int)
(declare-fun x_Tuple52 () Int)
(declare-fun head () Int)
(declare-fun lit$36$Game () Str)
(declare-fun fix$36$$36$krep_aTQ () Int)
(declare-fun lq_anf$36$$35$$35$7205759403792797121$35$$35$dUN () Int)
(declare-fun papp2 () Int)
(declare-fun Game.$36$tc$39$Healer () Int)
(declare-fun x_Tuple62 () Int)
(declare-fun lit$36$main () Str)
(declare-fun VV$35$$35$1474 () Int)
(declare-fun lit$36$$39$Point () Str)
(declare-fun fromJust () Int)
(declare-fun fix$36$$36$krep_aTT () Int)
(declare-fun GHC.Types.KindRepTyConApp () Int)
(declare-fun Game.$36$tc$39$Board () Int)
(declare-fun lq_tmp$36$x$35$$35$357 () Int)
(declare-fun papp7 () Int)
(declare-fun Game.$36$tc$39$Warrior () Int)
(declare-fun VV$35$$35$1642 () Int)
(declare-fun lq_tmp$36$x$35$$35$871 () Int)
(declare-fun x_Tuple53 () Int)
(declare-fun lq_anf$36$$35$$35$7205759403792797128$35$$35$dUU () Int)
(declare-fun lit$36$Role () Str)
(declare-fun VV$35$$35$1239 () Int)
(declare-fun GHC.Types.$91$$93$ () Int)
(declare-fun lit$36$$39$Wizard () Str)
(declare-fun x_Tuple71 () Int)
(declare-fun fix$36$$36$krep_aU2 () Int)
(declare-fun Game.Point () Int)
(declare-fun VV$35$$35$1588 () Int)
(declare-fun GHC.Types.GT () Int)
(declare-fun lq_tmp$36$x$35$$35$597 () Int)
(declare-fun lq_anf$36$$35$$35$7205759403792797144$35$$35$dVa () Int)
(declare-fun x_Tuple74 () Int)
(declare-fun lq_tmp$36$x$35$$35$975 () Int)
(declare-fun lq_anf$36$$35$$35$7205759403792797141$35$$35$dV7 () Int)
(declare-fun lq_tmp$36$x$35$$35$686 () Int)
(declare-fun fix$36$$36$krep_aTU () Int)
(declare-fun VV$35$$35$1431 () Int)
(declare-fun len () Int)
(declare-fun papp6 () Int)
(declare-fun lq_tmp$36$x$35$$35$1081 () Int)
(declare-fun lit$36$Board () Str)
(declare-fun x_Tuple22 () Int)
(declare-fun Game.$36$tcPoint () Int)
(declare-fun x_Tuple66 () Int)
(declare-fun x_Tuple44 () Int)
(declare-fun Game.$36$tc$39$Player () Int)
(declare-fun VV$35$$35$1215 () Int)
(declare-fun VV$35$$35$1407 () Int)
(declare-fun x_Tuple72 () Int)
(declare-fun lq_tmp$36$x$35$$35$500 () Int)
(declare-fun lq_tmp$36$x$35$$35$885 () Int)
(declare-fun Game.Board () Int)
(declare-fun fix$36$$36$krep_aU1 () Int)
(declare-fun fix$36$$36$krep_aTX () Int)
(declare-fun lq_tmp$36$x$35$$35$397 () Int)
(declare-fun isJust () Int)
(declare-fun lq_tmp$36$x$35$$35$1095 () Int)
(declare-fun lq_anf$36$$35$$35$7205759403792797134$35$$35$dV0 () Int)
(declare-fun lq_tmp$36$x$35$$35$306 () Int)
(declare-fun lq_tmp$36$x$35$$35$626 () Int)
(declare-fun x_Tuple31 () Int)
(declare-fun x_Tuple75 () Int)
(declare-fun lq_tmp$36$x$35$$35$406 () Int)
(declare-fun lq_tmp$36$x$35$$35$1021 () Int)
(declare-fun GHC.Types.TrNameS () Int)
(declare-fun VV$35$$35$1545 () Int)
(declare-fun papp1 () Int)
(declare-fun Game.$36$tc$39$Point () Int)
(declare-fun VV$35$$35$1450 () Int)
(declare-fun x_Tuple61 () Int)
(declare-fun lq_anf$36$$35$$35$7205759403792797112$35$$35$dUE () Int)
(declare-fun x_Tuple43 () Int)
(declare-fun tail () Int)
(declare-fun lit$36$$39$Board () Str)
(declare-fun lit$36$$39$Warrior () Str)
(declare-fun VV$35$$35$1274 () Int)
(declare-fun fix$36$$36$krep_aTR () Int)
(declare-fun GHC.Types.TyCon () Int)
(declare-fun fix$36$$36$krep_aTY () Int)
(declare-fun fix$36$$36$krep_aU0 () Int)
(declare-fun lq_anf$36$$35$$35$7205759403792797125$35$$35$dUR () Int)
(declare-fun x_Tuple51 () Int)
(declare-fun lq_tmp$36$x$35$$35$343 () Int)
(declare-fun x_Tuple73 () Int)
(declare-fun GHC.Types.EQ () Int)
(declare-fun Game.Player () Int)
(declare-fun fix$36$$36$krep_aU5 () Int)
(declare-fun lit$36$Player () Str)
(declare-fun VV$35$$35$1160 () Int)
(declare-fun VV$35$$35$1564 () Int)
(declare-fun x_Tuple54 () Int)
(declare-fun VV$35$$35$1623 () Int)
(declare-fun lq_tmp$36$x$35$$35$320 () Int)
(declare-fun x_Tuple32 () Int)
(declare-fun lq_anf$36$$35$$35$7205759403792797132$35$$35$dUY () Int)
(declare-fun VV$35$$35$1359 () Int)
(declare-fun x_Tuple76 () Int)
(declare-fun VV$35$$35$1383 () Int)
(declare-fun fix$36$$36$krep_aTS () Int)
(declare-fun lit$36$Point () Str)
(declare-fun lit$36$$39$Player () Str)
(declare-fun Game.$36$tcBoard () Int)
(declare-fun snd () Int)
(declare-fun fst () Int)
(declare-fun x_Tuple42 () Int)
(declare-fun apply$35$$35$13 (Int (_ BitVec 32)) Bool)
(declare-fun apply$35$$35$9 (Int Str) Bool)
(declare-fun apply$35$$35$6 (Int Bool) Str)
(declare-fun apply$35$$35$11 (Int Str) (_ BitVec 32))
(declare-fun apply$35$$35$15 (Int (_ BitVec 32)) (_ BitVec 32))
(declare-fun apply$35$$35$0 (Int Int) Int)
(declare-fun apply$35$$35$8 (Int Str) Int)
(declare-fun apply$35$$35$1 (Int Int) Bool)
(declare-fun apply$35$$35$7 (Int Bool) (_ BitVec 32))
(declare-fun apply$35$$35$14 (Int (_ BitVec 32)) Str)
(declare-fun apply$35$$35$10 (Int Str) Str)
(declare-fun apply$35$$35$5 (Int Bool) Bool)
(declare-fun apply$35$$35$2 (Int Int) Str)
(declare-fun apply$35$$35$12 (Int (_ BitVec 32)) Int)
(declare-fun apply$35$$35$3 (Int Int) (_ BitVec 32))
(declare-fun apply$35$$35$4 (Int Bool) Int)
(declare-fun smt_lambda$35$$35$13 ((_ BitVec 32) Bool) Int)
(declare-fun smt_lambda$35$$35$9 (Str Bool) Int)
(declare-fun smt_lambda$35$$35$6 (Bool Str) Int)
(declare-fun smt_lambda$35$$35$11 (Str (_ BitVec 32)) Int)
(declare-fun smt_lambda$35$$35$15 ((_ BitVec 32) (_ BitVec 32)) Int)
(declare-fun smt_lambda$35$$35$0 (Int Int) Int)
(declare-fun smt_lambda$35$$35$8 (Str Int) Int)
(declare-fun smt_lambda$35$$35$1 (Int Bool) Int)
(declare-fun smt_lambda$35$$35$7 (Bool (_ BitVec 32)) Int)
(declare-fun smt_lambda$35$$35$14 ((_ BitVec 32) Str) Int)
(declare-fun smt_lambda$35$$35$10 (Str Str) Int)
(declare-fun smt_lambda$35$$35$5 (Bool Bool) Int)
(declare-fun smt_lambda$35$$35$2 (Int Str) Int)
(declare-fun smt_lambda$35$$35$12 ((_ BitVec 32) Int) Int)
(declare-fun smt_lambda$35$$35$3 (Int (_ BitVec 32)) Int)
(declare-fun smt_lambda$35$$35$4 (Bool Int) Int)
(declare-fun lam_arg$35$$35$1$35$$35$0 () Int)
(declare-fun lam_arg$35$$35$2$35$$35$0 () Int)
(declare-fun lam_arg$35$$35$3$35$$35$0 () Int)
(declare-fun lam_arg$35$$35$4$35$$35$0 () Int)
(declare-fun lam_arg$35$$35$5$35$$35$0 () Int)
(declare-fun lam_arg$35$$35$6$35$$35$0 () Int)
(declare-fun lam_arg$35$$35$7$35$$35$0 () Int)
(declare-fun lam_arg$35$$35$1$35$$35$8 () Str)
(declare-fun lam_arg$35$$35$2$35$$35$8 () Str)
(declare-fun lam_arg$35$$35$3$35$$35$8 () Str)
(declare-fun lam_arg$35$$35$4$35$$35$8 () Str)
(declare-fun lam_arg$35$$35$5$35$$35$8 () Str)
(declare-fun lam_arg$35$$35$6$35$$35$8 () Str)
(declare-fun lam_arg$35$$35$7$35$$35$8 () Str)
(declare-fun lam_arg$35$$35$1$35$$35$12 () (_ BitVec 32))
(declare-fun lam_arg$35$$35$2$35$$35$12 () (_ BitVec 32))
(declare-fun lam_arg$35$$35$3$35$$35$12 () (_ BitVec 32))
(declare-fun lam_arg$35$$35$4$35$$35$12 () (_ BitVec 32))
(declare-fun lam_arg$35$$35$5$35$$35$12 () (_ BitVec 32))
(declare-fun lam_arg$35$$35$6$35$$35$12 () (_ BitVec 32))
(declare-fun lam_arg$35$$35$7$35$$35$12 () (_ BitVec 32))
(declare-fun lam_arg$35$$35$1$35$$35$4 () Bool)
(declare-fun lam_arg$35$$35$2$35$$35$4 () Bool)
(declare-fun lam_arg$35$$35$3$35$$35$4 () Bool)
(declare-fun lam_arg$35$$35$4$35$$35$4 () Bool)
(declare-fun lam_arg$35$$35$5$35$$35$4 () Bool)
(declare-fun lam_arg$35$$35$6$35$$35$4 () Bool)
(declare-fun lam_arg$35$$35$7$35$$35$4 () Bool)
(assert (distinct lit$36$$39$Player lit$36$Point lit$36$Player lit$36$$39$Warrior lit$36$$39$Board lit$36$Board lit$36$$39$Wizard lit$36$Role lit$36$$39$Point lit$36$main lit$36$Game lit$36$$39$Healer))
(assert (distinct GHC.Types.EQ GHC.Types.GT GHC.Types.LT))
(assert (= (strLen lit$36$$39$Healer) 7))
(assert (= (strLen lit$36$Game) 4))
(assert (= (strLen lit$36$main) 4))
(assert (= (strLen lit$36$$39$Point) 6))
(assert (= (strLen lit$36$Role) 4))
(assert (= (strLen lit$36$$39$Wizard) 7))
(assert (= (strLen lit$36$Board) 5))
(assert (= (strLen lit$36$$39$Board) 6))
(assert (= (strLen lit$36$$39$Warrior) 8))
(assert (= (strLen lit$36$Player) 6))
(assert (= (strLen lit$36$Point) 5))
(assert (= (strLen lit$36$$39$Player) 7))
(exit)
