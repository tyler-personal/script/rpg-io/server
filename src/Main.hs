module Main () where

import Control.Monad
import Network.Simple.TCP
import Data.ByteString.Char8

byteLimit = 4096

main =
  listen HostAny "9000" $ \(listenSocket, listenAddress) -> do
    print $ "Listening for TCP connections at " ++ show listenAddress
    forever . acceptFork listenSocket $ \(acceptSocket, acceptAddress) -> do
      print $ "Accepted incoming connection from " ++ show acceptAddress
      echoloop acceptSocket
  where 
    echoloop socket = do
      possibleData <- recv socket byteLimit
      case possibleData of
        Just realData -> send socket (formatResult realData) >> echoloop socket
        Nothing -> return ()

formatResult result = pack(unpack result ++ "\n")

