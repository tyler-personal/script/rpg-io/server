module Game where

data BoardState = BoardState
  { players :: [Player]
  } 

type Point = (Nat, Nat)

data Board = Board 
  { points :: Points 
  } deriving (Ord)

data Role = Warrior | Wizard | Healer

data Player = Player 
  { name :: String
  , role :: Role
  , position :: Point 
  , maxHealth :: Int
  , currentHealth :: Int
  }

